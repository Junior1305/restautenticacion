<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Usuario;
use Validator; 

class UsuarioController extends BaseController
{

	public function __construct(){
		$this->middleware('cors');
	}

	public function index()
	{
		$usuarios = Usuario::all();
		return $this->sendResponse($usuarios->toArray(), 'Usuarios enviados exitosamente.');
	}


    public function list()
    {
        
        $usuarios = Usuario::where('us_estado','1')->whereIn('perfil_id',[2,3])->get();
        return $this->sendResponse($usuarios->toArray(), 'Usuarios enviados exitosamente.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$input = \json_decode($request->getContent(), true);
    	$input['us_registerDate'] = date('Y-m-d H:i:s');
    	$input['us_registerUpdate'] = date('Y-m-d H:i:s');
    	
    	if($input['us_login'] == '' || $input['us_pass'] == '' || $input['us_docIdentidad'] == '' ){
    		return $this->sendError('','Usuario no registrado.');       
    	}

    	$usuario = Usuario::create($input);


    	return $this->sendResponse($usuario->toArray(), 'Usuario creado exitosamente.');
    }

    public function login(Request $request)
    {
        $input = \json_decode($request->getContent(), true);
        if($input['us_tipo'] == '2'){
            $usuario = Usuario::where('us_login',$input['us_login'])->where('us_pass',$input['us_pass'])->where('us_estado','1')->where('perfil_id',2)->get();
        }else{
            $usuario = Usuario::where('us_login',$input['us_login'])->where('us_pass',$input['us_pass'])->where('us_estado','1')->whereNotIn('perfil_id',[2])->get();
        }

        if (count($usuario)==0) {
            return $this->sendError($usuario->toArray(),'Usuario no encontrado.');
        }

        return $this->sendResponse($usuario->toArray(), 'Usuario logueado exitosamente.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $input = \json_decode($request->getContent(), true);

    	$usuario = Usuario::find($input['id']);

    	if (is_null($usuario)) {
    		return $this->sendError(null,'Usuario no encontrado.');
    	}


    	return $this->sendResponse($usuario->toArray(), 'Usuario enviado exitosamente.');
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = \json_decode($request->getContent(), true);
        $usuario = Usuario::find($input['id']);
        $input['us_registerUpdate'] = date('Y-m-d H:i:s');
        if (is_null($usuario)) {
            return $this->sendError($usuario->toArray(),'Usuario no encontrado.');
        }

    	$usuario->us_login = $input['us_login'];
    	$usuario->us_pass = $input['us_pass'];
    	$usuario->us_docIdentidad = $input['us_docIdentidad'];
    	$usuario->us_nombre = $input['us_nombre'];
        $usuario->us_apellido = $input['us_apellido'];
        $usuario->us_razonSocial = $input['us_razonSocial'];
        $usuario->us_direccion = $input['us_direccion'];
        $usuario->us_email = $input['us_email'];
        $usuario->us_telefono = $input['us_telefono'];
        $usuario->us_estado = $input['us_estado'];
        $usuario->us_registerUpdate = $input['us_registerUpdate'];
        $usuario->us_userUpdate = $input['us_userUpdate'];

    	$usuario->save();


    	return $this->sendResponse($usuario->toArray(), 'Usuario actualizado exitosamente.');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = \json_decode($request->getContent(), true);
        $usuario = Usuario::find($input['id']);
    	$usuario->delete();
    	return $this->sendResponse($usuario->toArray(), 'Usuario eliminado exitosamente.');
    }
}
