<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    public $table = "perfil";
    public $timestamps = false;
    protected $fillable = [
    	'perf_nombre','perf_estado','perf_registerDate','perf_registerUpdate','perf_userUpdate'
    ];

}
