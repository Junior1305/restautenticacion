<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $table = "usuario";
	public $timestamps = false;
    protected $fillable = [
    	'perfil_id','us_login','us_pass','us_docIdentidad','us_nombre','us_apellido','us_razonSocial','us_direccion','us_email','us_telefono','us_estado','us_registerDate','us_registerUpdate','us_userUpdate'
    ];



}
