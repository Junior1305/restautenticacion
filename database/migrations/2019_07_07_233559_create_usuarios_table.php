<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('perfil_id');
            $table->foreign('perfil_id')->references('id')->on('perfil');
            $table->string('us_login',50);
            $table->string('us_pass',50);
            $table->string('us_docIdentidad',15);
            $table->string('us_nombre',100);
            $table->string('us_apellido',100);
            $table->string('us_razonSocial',500);
            $table->string('us_direccion',500);
            $table->string('us_email',100);
            $table->string('us_telefono',30);
            $table->char('us_estado',1);
            $table->dateTime('us_registerDate');
            $table->dateTime('us_registerUpdate');
            $table->char('us_userUpdate',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
