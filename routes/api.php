<?php

use Illuminate\Http\Request;
use App\Usuario;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'cors'],function() {
	Route::post('login','UsuarioController@login');
	Route::post('getUser','UsuarioController@show');
	Route::post('createUser','UsuarioController@store');
	Route::get('listUser','UsuarioController@list');
	Route::delete('deleteUser','UsuarioController@destroy');
	Route::put('updateUser','UsuarioController@update');
});
